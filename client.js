var thrift = require("thrift");
/*
 * 匯入 thrift產生的函式庫
 */
var DemoApi = require("./gen-nodejs/DemoApi");
/*
 * 匯入 thrift產生的結構定義檔
 */
var ttypes = require("./gen-nodejs/demo_types");
/**
 * 通訊協定定義
 */
var transport = thrift.TBufferedTransport();
var protocol = thrift.TBinaryProtocol();

/**
 * 連線
 */
var connection = thrift.createConnection("localhost", 8080, {
    transport: transport,
    protocol: protocol
});

/**
 * 連線錯誤事件
 */
connection.on('error', function (err) {
    console.error(err);
});

/**
 * 打開一個DemoApi服務通訊流程
 */
var client = thrift.createClient(DemoApi, connection);

/**
 * 最單純的通訊，呼叫定義的apiCall方法，定義的方法請參照demo.thrift
 */
client.apiCall(function (err, response) {
    console.log('apiCall()');
});

/**
 * 數值資料通訊
 */
client.addNum(1, 1, function (err, response) {
    console.log("1+1=" + response);
});

/**
 *  字串資料通訊
 */
client.getName('cain', function (err, resName) {
    console.log(resName);
});

/**
 * Json通訊，欄位不能動態，必須事先於.thrift檔案中定義結構，動態json請參照下方範例的jsonCoding方法
 */
client.getJson(function (err, json) {
    console.log(json);
})

/**
 *Json通訊(含陣列)，欄位不能動態，必須事先於.thrift檔案中定義結構，動態json請參照下方範例的jsonCoding方法
 */
client.getJson2(function (err, json) {
    console.log(json);
})

/**
 * 物件通訊，物件的結構須先於.thrift檔案中定義結構
 */
client.getTable(function (err, data) {
    console.log(data);
})

/**
 * 物件通訊(單向)，物件的結構須先於.thrift檔案中定義結構
 */
var table = new ttypes.DemoTable();
table.id = 5;
table.name = 'post string';
client.post(table);
var ob = {
    name: 'jsonCoding',
    data: {
        a: 'xxx',
        b: 'cccc'
    }
}

/**
 * 如需通訊json資料，目前做法是用字串格式處理
 */
client.jsonCoding(JSON.stringify(ob), function (err, jsonString) {
    console.log(JSON.parse(jsonString));
    connection.end(); //事情都作完記得關閉連線，很重要!!!
});

// setTimeout(function () {
//     connection.end();
//     console.log('connection end!');
// }, 1000)