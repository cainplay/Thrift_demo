# 使用Thrift於Node.js服務間通訊

## [Thrift官網](https://thrift.apache.org)

## 範例位置(gitlab)
[http://192.168.1.3:8888/demo/Thrift_demo](http://192.168.1.3:8888/demo/Thrift_demo)

## 使用方式

### 1.安裝npm thrift
```bash
npm i thrift
```

### 2.下載thrift編譯器

[Thrift編譯器](https://thrift.apache.org/download)

編譯器可將介面定義檔(.thrift)編譯成各語言的介面函示庫

### 3.編譯介面定義檔

編譯範例介面定義檔 demo.thrift
```bash
thrift -r --gen js:node demo.thrift
```

編譯完成後產生介面函示庫**gen-nodejs**資料夾

### 4.啟動服務

[server](http://192.168.1.3:8888/demo/Thrift_demo/blob/master/server.js)
```bash
node server.js
```

[client](http://192.168.1.3:8888/demo/Thrift_demo/blob/master/client.js)
```bash
node client.js
```

啟動client端後，應見到測試正常輸出

## Thrift定義檔說明

### 可定義的基本類型

* bool
* i8 (byte)
* i16(16位元整數)
* i32(32位元整數)
* i64(64位元整數)
* double(64位元浮點數)
* string(字串)
* binary(byte array)

### 可定義的結構

* map
* list
* struct

請見 demo.thrift 範本
[demo.thrift](http://192.168.1.3:8888/demo/Thrift_demo/blob/master/demo.thrift)

官方範例在apacheDemo資料夾

