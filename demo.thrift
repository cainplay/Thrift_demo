/**
 * 介面定義檔
 */

/* 各語言命名空間 這邊不用改*/
namespace cpp demo
namespace d demo
namespace java demo
namespace php demo
namespace perl demo
namespace haxe demo


/* 定義一個 DemoTable 結構，兩個值 id(i32) name(string) */
struct DemoTable {
  1: i32 id = 0,
  2: string name
}

/* 定義呼叫的方法介面與參數 */
service DemoApi {
	void apiCall(),
	i32 addNum(1:i32 num1, 2:i32 num2),
	string getName(1:string myName),
	map<string,string> getJson(),
	map<string,list<double>> getJson2(),
	DemoTable getTable(),
	oneway void post(1:DemoTable data),  /* oneway 單向通訊，不回傳值請加上 void */
	string jsonCoding(1:string jsonString)
}