var thrift = require("thrift");
/*
 * 匯入 thrift產生的函式庫
 */
var DemoApi = require("./gen-nodejs/DemoApi");
/*
 * 匯入 thrift產生的結構定義檔
 */
var ttypes = require("./gen-nodejs/demo_types");

/**
 * 定義一個 DemoApi 服務器
 */
var server = thrift.createServer(DemoApi, {
    /**
     * 最單純的通訊，呼叫定義的apiCall方法，定義的方法請參照demo.thrift
     */
    apiCall: function (result) {
        console.log("apiCall()");
        result(null);
    },
    /**
     * 數值資料通訊
     */
    addNum: function (n1, n2, result) {
        console.log("add(", n1, ",", n2, ")");
        result(null, n1 + n2);
    },
    /**
     * 字串資料通訊
     */
    getName: function (myName, result) {
        console.log("getName(", myName, ")");
        result(null, myName + ' Chu');
    },
    /**
     * Json通訊，欄位不能動態，必須事先於.thrift檔案中定義結構，動態json請參照下方範例的jsonCoding方法
     */
    getJson: function (result) {
        console.log("getJson()");
        var ob = { 'hello': 'world', 'goodnight': 'moon' };
        result(null, ob);
    },
    /**
     * Json通訊(含陣列)，欄位不能動態，必須事先於.thrift檔案中定義結構，動態json請參照下方範例的jsonCoding方法
     */
    getJson2: function (result) {
        console.log("getJson2()");
        var ob = { 'numList': [1, 3, 5, 7, 9], 'numList2': [2.2, 5.5, 8.7, 10.11, -2.3] };
        result(null, ob);
    },
    /**
     * 物件通訊，物件的結構須先於.thrift檔案中定義結構
     */
    getTable: function (result) {
        console.log("getTable()");
        var table = new ttypes.DemoTable(); //DemoTable定義於.thrift檔案中
        table.id = 2;
        table.name = 'table string';
        result(null, table);
    },
    /**
     * 單向通訊(沒有result)
     */
    post: function (data) {
        console.log(data);
    },
    /**
     * 如需通訊json資料，目前做法是用字串格式處理
     */
    jsonCoding: function (jsonString, result) {
        console.log(JSON.parse(jsonString));
        var ob = {
            name: 'jsonCoding',
            arr: [1, 2, 3, 4, 5]
        }
        result(null, JSON.stringify(ob));
    }
});

/**
 * 偵聽8080 port
 */
server.listen(8080);